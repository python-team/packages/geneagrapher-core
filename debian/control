Source: geneagrapher-core
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Doug Torrance <dtorrance@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dh-sequence-python3,
               dh-sequence-sphinxdoc,
               pybuild-plugin-pyproject,
               python3-aiohttp,
               python3-all,
               python3-bs4,
               python3-poetry-core,
               python3-pytest,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-autodoc-typehints,
               python3-sphinx-rtd-theme
Standards-Version: 4.6.2
Homepage: https://github.com/davidalber/geneagrapher-core
Vcs-Browser: https://salsa.debian.org/python-team/packages/geneagrapher-core
Vcs-Git: https://salsa.debian.org/python-team/packages/geneagrapher-core.git
Testsuite: autopkgtest-pkg-pybuild
Rules-Requires-Root: no

Package: python3-geneagrapher-core
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: request and build graphs of records from the Math Genealogy Project
 Geneagrapher is a tool for extracting information from the
 Mathematics Genealogy Project to form a math family tree, where
 connections are between advisors and their students.
 .
 This package contains the core data-grabbing and manipulation
 functions needed to build a math family tree. The functionality here
 is low level and intended to support the development of other
 tools. If you just want to build a geneagraph, take a look at
 Geneagrapher. If you want to get mathematician records and use them in
 code, then this project may be useful to you.
